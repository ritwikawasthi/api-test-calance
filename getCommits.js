// DEPENDENCIES
const axios = require('axios'); //HTTP Requests
const fs = require('fs'); // File I/O

//GLOBALS
const BitbucketURL =
  'https://api.bitbucket.org/2.0/repositories/calanceus/api-test/commits/?fields=values.message';

const filePath = './commitMessages.txt';

// getCommits - calls Bitbucket api to get values.message field from commits, and then writes them to a file
async function getCommits() {
  //CHECK IF FILE EXISTS
  try {
    //IF TRUE
    if (fs.existsSync(filePath)) {
      //DELETE OLD FILE
      fs.unlinkSync(filePath);
      console.log('OLD FILE DELETED...');
    }
  } catch (err) {
    console.log('FILE ERROR: ');
    console.log(err);
  }

  console.log('FETCHING MESSAGES FROM BITBUCKET API...');
  //GET REQUEST
  res = await axios.get(BitbucketURL);

  //CHECK IF RESPONSE IS EMPTY
  if (!res.data) {
    console.log('RESPONSE EMPTY');
  } else {
    console.log('PARSING RESPONSE...');
    const { values } = res.data; // DESTRUCTURE

    console.log('CREATING NEW FILE...');

    //LOOP THROUGH MESSAGES IN ORDER
    for (let msg of values) {
      //APPEND TO FILE
      fs.appendFileSync('commitMessages.txt', msg.message, function(err) {
        if (err) {
          throw err;
        }
      });
    }

    console.log('FILE UPDATED!');
  }
}

getCommits();
